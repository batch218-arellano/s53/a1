import {Container} from 'react-bootstrap';
import React from "react";
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import './App.css';


import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';

function App() {
  return (
    // Common pattern in react.js for a component to return multiple elements.
    <>
    <Router>
    <AppNavbar />
    <Container>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/courses" element={<Courses />} />
         <Route path="/register" element={<Register />} />
         <Route path="/login" element={<Login />} />
         <Route path="/logout" element={<Logout />} />
         <Route path="/*" element={<Error />} />
         { /*
          "*" - wildcard character that will match any path that was defined in routes. 
         */ }
      </Routes>
    </Container> 
    </Router>
    </>
  );
}


export default App;

